package com.itau.maratona;

import java.util.List;

public class App {

	public static void main(String[] args) {
		Arquivo arquivo = new Arquivo("src/alunos.csv");
		List<Aluno> alunos = arquivo.ler();

		System.out.println("Alunos (" + alunos.size() + "): " + alunos);
		Maratona maratona = new Maratona(alunos, 3);
		maratona.montaEquipes();
	}
}