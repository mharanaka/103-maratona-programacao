package com.itau.maratona;

import java.util.ArrayList;

public class Equipe {
	private int id;
	private ArrayList<Aluno> membros = new ArrayList<Aluno>();

	public Equipe(int id) {
		this.id = id;
	}

	public void adicionaAluno(Aluno aluno) {
		System.out.println("Adicionado aluno: " + aluno + " na equipe.");
		this.membros.add(aluno);
		System.out.println("Total da equipe " + id + ": " + membros.size() + ".\n");
	}

	public ArrayList<Aluno> getMembros() {
		return membros;
	}

	@Override
	public String toString() {

		return "Equipe " + id;
	}
}