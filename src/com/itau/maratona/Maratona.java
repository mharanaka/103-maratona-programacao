package com.itau.maratona;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Maratona {

	private List<Aluno> participantes = new ArrayList<Aluno>();
	private List<Equipe> equipes = new ArrayList<Equipe>();
	private int participantesPorEquipe = 0;

	public Maratona(List<Aluno> participantes, int participantesPorEquipe) {

		if (participantes.size() <= 0 || participantesPorEquipe <= 0) {
			throw new IllegalArgumentException(
					"A quantidade de alunos e o número de participantes por equipe devem ser maior que zero.");
		}

		this.participantes = participantes;
		this.participantesPorEquipe = participantesPorEquipe;
		this.defineTotalDeEquipes();
	}

	private void defineTotalDeEquipes() {

		// Define quantos grupos devem existir dividindo pelo número de integrantes informado. Se o resto da operação for maior que
		// zero soma mais um grupo.
		int totalEquipes = (int) Math.ceil(participantes.size() / this.participantesPorEquipe);
		if (participantes.size() % this.participantesPorEquipe > 0)
			totalEquipes++;

		System.out.println("Total de equipes: " + totalEquipes + " definidas.\n");
		for (int i = 0; i < totalEquipes; i++) {
			equipes.add(new Equipe(i + 1));
		}
	}

	private Aluno sorteiaAluno() {
		Aluno alunoRemovido = null;
		if (participantes.size() > 0) {
			Random rand = new Random();
			alunoRemovido = participantes.get(rand.nextInt(participantes.size()));
			participantes.remove(alunoRemovido);
			System.out.println(
					"Removido aluno: " + alunoRemovido + " da lista total ficando (" + participantes.size() + ").");
		}

		return alunoRemovido;
	}

	public List<Equipe> montaEquipes() {
		for (int i = 0; i < participantes.size(); i++) {
			for (Equipe e : equipes) {
				System.out.println("Adicionando aluno na " + e + ":");
				e.adicionaAluno(this.sorteiaAluno());
			}
			System.out.println("\n");
		}
		return equipes;
	}
}
